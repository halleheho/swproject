package Model;

import java.util.Random;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

public class Customer  implements Actionable{
	
	private TeaConcept request;
	private Random rand = new Random();
	private String character = "";

	
	public Customer(){
		request = new TeaConcept();
		createRequest();
		
	}
	


	
	
	public String randomChar(){
		int random = rand.nextInt(4)+1;
		if(("image/cus"+random+".gif").equals(getCharacter())){
			return randomChar();
		}else{
			return "image/cus"+random+".gif";
		}
	}
	
	
	void createRequest(){
		character = randomChar();
		request.setTeaType(randomTeaType());
		request.setSweetness(randomSweetness());
		request.setTopping(randomTopping());
		System.out.println("TeaType = "+request.getTeaType());
		System.out.println("Sweetness = "+request.getSweetness());
		System.out.println("Topping = "+request.getTopping());
		
	}
	
	
	public String getCharacter() {
		return character;
	}


	public int randomSweetness(){
		int x = rand.nextInt(5)+1;
		return x;

	}
	
	
	public String randomTeaType(){
		int x = rand.nextInt(4);
		if(x == 0)
			return "cocoa";
		else if(x == 1)
			return "milktea";
		else if(x == 2)
			return "strawberry";
		else 
			return "greentea";
	}
	
	public String randomTopping(){
		
		int x = rand.nextInt(4);
		
		if(x == 0)
			return "no";
		else if(x == 1)
			return "pudding";
		else if(x == 2)
			return "bubble";
		else 
			return "jelly";
	}
	

	/**
	 * No use for this class.
	 */
	@Override
	public String toString(){
		return request.toString();
	}
	/**
	 * No use for this class.
	 */
	@Override
	public void doTeaCupAction(Tea cup) {
		// TODO Auto-generated method stub
		
	}
	
	public TeaConcept getRequest() {
		return request;
	}
}
