package Model;

public class ScoreCounter {

	private int score = 0;
	
	public ScoreCounter() {
		
	}
	
	public int getScore(){
		return score;
	}
	
	public void increase(){
		score += 250;
	}
	
	public void reset(){
		this.score = 0;
	}
}
