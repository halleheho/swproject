package Model;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

public class HowToComponent extends AnimatedObject {
	
	String initial,animated;
	JLabel image;

	public HowToComponent(String initialImg,String animatedImg){
		this.initial = initialImg;
		this.animated = animatedImg;
		image = new JLabel(new ImageIcon("image/howto/"+initialImg+".png"));
		image.addMouseListener(this);
	}
	
	@Override
	protected void doPressedAction() {
		setChanged();
		notifyObservers(initial+"s");
	}

	@Override
	protected void doEnterAction() {
		// TODO Auto-generated method stub
		image.setIcon(new ImageIcon("image/howto/"+animated+".png"));
	}

	@Override
	protected void doExitAction() {
		image.setIcon(new ImageIcon("image/howto/"+initial+".png"));
	}

	public JLabel getImage(){
		return image;
	}

}
