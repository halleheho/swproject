package Model;

import java.io.IOException;

import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

public class WrongOperationStepException extends RuntimeException {

	public WrongOperationStepException(String string) {
		super(string);
		try {
			new Sound("song/cartoon006.wav");
		} catch (UnsupportedAudioFileException | IOException
				| LineUnavailableException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 295909991331130290L;
}
