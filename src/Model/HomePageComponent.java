package Model;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

public class HomePageComponent extends AnimatedObject  {
	
	String initial,animated;
	JLabel image;

	public HomePageComponent(String initialImg,String animatedImg){
		this.initial = initialImg;
		this.animated = animatedImg;
		image = new JLabel(new ImageIcon("image/home/"+initialImg+".png"));
		image.addMouseListener(this);
	}
	
	@Override
	protected void doPressedAction() {
		setChanged();
		notifyObservers(initial+"s");
	}

	@Override
	protected void doEnterAction() {
		// TODO Auto-generated method stub
		image.setIcon(new ImageIcon("image/home/"+animated+".png"));
	}

	@Override
	protected void doExitAction() {
		image.setIcon(new ImageIcon("image/home/"+initial+".png"));
	}

	public JLabel getImage(){
		return image;
	}

}
