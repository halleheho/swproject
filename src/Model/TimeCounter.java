package Model;

import java.awt.Toolkit;
import java.io.IOException;
import java.util.Observable;
import java.util.Timer;
import java.util.TimerTask;

import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

public class TimeCounter extends Observable {

	Toolkit toolkit;
	Timer timer;

	public TimeCounter() {
		toolkit = Toolkit.getDefaultToolkit();
		timer = new Timer();
		timer.schedule(new RemindTask(), 0, // initial delay
				1 * 1000); // subsequent rate
	}

	class RemindTask extends TimerTask {
		int time = 90;

		public void run() {
			if (time >= 0) {
				setChanged();
				notifyObservers(time);
				time--;
			} else {
				System.out.format("Time's up!%n");
				try {
					new Sound("song/timeup.wav");
					setChanged();
					notifyObservers("STOP");
				} catch (UnsupportedAudioFileException | IOException
						| LineUnavailableException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				timer.cancel();
			}
		}
	}

}
