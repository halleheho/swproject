package Model;

public class Sign extends SmartObject implements Actionable{
	
	String topping;
	
	public Sign(String initialImg, String animatedImg, Tea cup,String topping) {
		super(initialImg, animatedImg, cup);
		this.topping = topping;
	}

	@Override
	public void doTeaCupAction(Tea cup) {
		// TODO Auto-generated method stub
		try {
			cup.setTopping(topping);
		} catch (WrongOperationStepException e) {
			// TODO: handle exception
			System.out.println(e);
		}
	}

	
}
