package Model;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Observable;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

public class SmartObject extends AnimatedObject{
	
	private Tea cup;
	protected JLabel image;
	protected String initialImg,animatedImg;
	
	public SmartObject(){
		
	}
	
	
	public SmartObject(String initialImg,String animatedImg){
		image = new JLabel();
		this.initialImg = initialImg;
		this.animatedImg = animatedImg;
		image.setIcon(new ImageIcon(initialImg));
		image.addMouseListener(this);
	}
	
	public SmartObject(String initialImg,String animatedImg,Tea cup){
		image = new JLabel();
		this.initialImg = initialImg;
		this.animatedImg = animatedImg;
		this.cup = cup;
		image.setIcon(new ImageIcon(initialImg));
		image.addMouseListener(this);
	}
	

	public Tea getCup(){
		return cup;
	}

	
	
	public JLabel getImage(){
		return image;
	}

	@Override
	protected void doPressedAction() {
		// TODO Auto-generated method stub
		setChanged();
		notifyObservers(this);
	}

	@Override
	protected void doEnterAction() {
		// TODO Auto-generated method stub
		image.setIcon(new ImageIcon(animatedImg));

	}

	@Override
	protected void doExitAction() {
		// TODO Auto-generated method stub
		image.setIcon(new ImageIcon(initialImg));
	}

	

}
