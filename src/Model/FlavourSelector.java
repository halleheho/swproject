package Model;

import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;

public class FlavourSelector extends SmartObject implements Actionable{
	
	String flavour;
	
	public FlavourSelector(String initialImg, String animatedImg,Tea cup,String flavour) {
		super(initialImg, animatedImg,cup);
		this.flavour = flavour;
		// TODO Auto-generated constructor stub
	}

	@Override
	public void doTeaCupAction(Tea cup) {
		try {
			cup.setTeaType(flavour);
		} catch (WrongOperationStepException e) {
			System.out.println(e);
		}
	}
	
}
