package Model;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.net.URL;
import javax.sound.sampled.*;
import javax.swing.*;

// To play sound using Clip, the process need to be alive.
// Hence, we use a Swing application.
public class Sound {

	JButton b;
	AudioInputStream audioIn;
	Clip clip;
	File bgm;

	// Constructor
	public Sound(String filename) throws UnsupportedAudioFileException, IOException,
			LineUnavailableException {
		
		bgm = new File(filename);
		clip = AudioSystem.getClip();
		audioIn = AudioSystem.getAudioInputStream(bgm);
		clip.open(audioIn);
		clip.start();

	}

	public Clip getClip(){
		return clip;
	}
	
}