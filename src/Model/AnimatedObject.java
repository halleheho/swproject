package Model;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Observable;

public abstract class AnimatedObject extends Observable implements MouseListener{

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		doPressedAction();
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		doEnterAction();
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		doExitAction();
	}
	/**
	 * Hook Method when the object is clicked.
	 */
	protected abstract void doPressedAction();

	/**
	 * Hook Method when the object is hovered.
	 */
	protected abstract void doEnterAction();

	/**
	 * Hook Method when the mouse exit from this object.
	 */
	protected abstract void doExitAction();
}
