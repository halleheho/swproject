package Model;

import java.awt.event.MouseEvent;
import java.io.IOException;

import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.ImageIcon;

public class HoneyBottle extends SmartObject implements Actionable{
	
	private String animatedImg,initialImg;
	
	
	public HoneyBottle(String initialImg, String animatedImg, Tea cup) {
		super(initialImg, animatedImg, cup);
		this.animatedImg = animatedImg;
		this.initialImg = initialImg;
	}

	
	@Override
	protected void doEnterAction() {
		// TODO Auto-generated method stub
		//ReOverride to do nothing for this case.
	}
	
	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		getImage().setIcon(new ImageIcon(animatedImg));
		try {
			new Sound("song/honeydrop.wav");
		} catch (UnsupportedAudioFileException | IOException
				| LineUnavailableException e1) {
			e1.printStackTrace();
		}
	}
	
	
	
	@Override
	public void doTeaCupAction(Tea cup) {
		// TODO Auto-generated method stub
		
		try {
			cup.addSweetness();
		} catch (WrongOperationStepException e) {
			System.out.println(e);
		}
		getImage().setIcon(new ImageIcon(initialImg));
	}



}
