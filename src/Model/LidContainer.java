package Model;

public class LidContainer extends SmartObject implements Actionable {

	public LidContainer(String initialImg, String animatedImg, Tea cup) {
		super(initialImg, animatedImg, cup);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void doTeaCupAction(Tea cup) {
		// TODO Auto-generated method stub
		try {
			cup.sealCup();
			System.out.println("SEAL CUP");
		
		} catch (WrongOperationStepException e) {
			System.out.println(e);
		}
	}

}
