package Model;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

public class Tea extends TeaConcept{
	private JLabel cupImg,toppingImg,lidImg;
	protected final int INITIAL_POSITION[] = {183,395};
	protected final int TRAY_POSITION[] = {430,506};
	private int state = 0;
	
	
	public Tea(){
		cupImg = new JLabel();
		toppingImg = new JLabel();
		lidImg = new JLabel();
		setCupLocation(INITIAL_POSITION);
		setToppingLocation(TRAY_POSITION);
		setLidLocation(TRAY_POSITION);
		this.topping = "no";
		this.teaType = "";
		this.sweetness = 0;
	}
	
	
	@Override
	public void reset() {
		super.reset();
		state = 0;
		getCup().setIcon(null);
		getToppingImg().setIcon(null);
		getLidImg().setIcon(null);
        setCupLocation(INITIAL_POSITION);
	}
	
	public void sealCup(){
		if(state == 4 || state == 3){
			if(this.getTopping().equals("no")){
				System.out.println("No Topping");
				getLidImg().setIcon(new ImageIcon("image/lidwithoutTopping.png"));
			}
			else
				getLidImg().setIcon(new ImageIcon("image/lidwithTopping.png"));
			return;
		}
		throw new WrongOperationStepException("Choose flavour or Topping first!");
	}

	public void addSweetness() {
		if(state == 2 || state == 3){
			this.sweetness++;
			state = 3;
			return;
		}
		throw new WrongOperationStepException("Choose Flavour first!");
		
	}
	
	@Override
	public void setTopping(String topping){
		if(state == 3){
			super.setTopping(topping);
			getToppingImg().setIcon(new ImageIcon("image/"+topping+".png"));
			state = 4;
			return;
		}
		throw new WrongOperationStepException("can't select more than 1 topping");
	}
	
	
	@Override
	public void setTeaType(String teaType) {
		if (state == 1) {
			super.setTeaType(teaType);
			getCup().setIcon(new ImageIcon("image/"+teaType+"Cup.png"));
			setCupLocation(TRAY_POSITION);
			state = 2;
			return;
		}
		throw new WrongOperationStepException("You selected tea or not selected cup");
	}
	
	

	public void setInitialPic() {
		if (state == 0) {
			getCup().setIcon(new ImageIcon("image/cup.png"));
			state = 1;
			return;
		}
		throw new WrongOperationStepException("You selected a cup, do not select again");
	}
	
	
	public void setCupLocation(int[] position){
		cupImg.setBounds(position[0], position[1],64,84);
	}
	
	public void setToppingLocation(int[] TRAY_POSITION){
		toppingImg.setBounds(TRAY_POSITION[0] + 6, TRAY_POSITION[1] - 20, 54, 27);
	}
	
	public void setLidLocation(int[] TRAY_POSITION){
		lidImg.setBounds(TRAY_POSITION[0] + 2, TRAY_POSITION[1]-48, 62, 53);
	}
	
	
	
	
	
	public JLabel getCup(){
		return cupImg;
	}
	
	public JLabel getToppingImg(){
		return toppingImg;
	}
	
	public JLabel getLidImg(){
		return lidImg;
	}
	
	
	
}
