package Model;

import java.io.IOException;

import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

public class CustomerAction extends SmartObject implements Actionable{

	private ScoreCounter score;
	private Customer customer;
	private Checker checker;
	private JLabel topping,flavour,sweetness;
	
	

	public CustomerAction(String initialImg, String animatedImg, Tea cup,Customer customer,JLabel topping,JLabel flavour,JLabel sweetness) {
		super(initialImg, animatedImg, cup);
		this.customer = customer;
		this.topping = topping;
		this.flavour = flavour;
		this.sweetness = sweetness;
		checker = new Checker();
		score = new ScoreCounter();
	}

	
	@Override
	public void doTeaCupAction(Tea cup) {
		
		if (checker.check(cup, customer)){
			score.increase();
			try {
				new Sound("song/correct.wav");
			} catch (UnsupportedAudioFileException | IOException
					| LineUnavailableException e) {
				System.out.println(e);
			}
//			JOptionPane.showMessageDialog(null, "Correct");
		}
		else{
			try {
				new Sound("song/wrong.wav");
			} catch (UnsupportedAudioFileException | IOException
					| LineUnavailableException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
//			JOptionPane.showMessageDialog(null, "Wrong");
		}

		cup.reset();
	
		customer.createRequest();
		
		flavour.setIcon(new ImageIcon("image/" + customer.getRequest().getTeaType() + ".png"));
		topping.setIcon(new ImageIcon("image/"+customer.getRequest().getTopping()+".png"));
		sweetness.setIcon(new ImageIcon("image/" + customer.getRequest().getSweetness() + ".png"));
	
		
		
		this.initialImg = customer.getCharacter();
		image.setIcon(new ImageIcon(initialImg));
	}
	
	
	@Override
	protected void doEnterAction() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void doExitAction() {
		// TODO Auto-generated method stub
	}

	public ScoreCounter getScoreCounter(){
		return score;
	}

}
