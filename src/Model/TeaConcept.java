package Model;

public class TeaConcept {
	protected String topping;		// no,bubble,pudding,jelly
	protected String teaType;		// milk,coco,Straw,Green
	protected int sweetness; 		// 1,2,3,4,5

	public void reset(){
		
		topping = "no";
		teaType = "";
		sweetness = 0;
	}
	
	public String getTopping() {
		return topping;
	}

	public void setTopping(String bubble) {
		this.topping = bubble;
	}

	public String getTeaType() {
		return teaType;
	}

	public void setTeaType(String teaType) {
		this.teaType = teaType;
	}

	public int getSweetness() {
		return sweetness;
	}

	public void setSweetness(int sweetness) {
		this.sweetness = sweetness;
	}
	


	@Override
	public String toString(){
		return "Toping = "+topping+" Flavour = "+teaType+" Sweetness = "+sweetness;
	}

	
}
