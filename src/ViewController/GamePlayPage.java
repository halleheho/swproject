package ViewController;

import java.awt.Color;
import java.awt.Font;

import java.io.IOException;
import java.util.Observable;
import java.util.Observer;

import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.ImageIcon;

import javax.swing.JLabel;

import Model.Bin;
import Model.Checker;
import Model.CupStack;
import Model.Customer;
import Model.CustomerAction;
import Model.FlavourSelector;
import Model.HoneyBottle;
import Model.LidContainer;
import Model.Sign;
import Model.SmartObject;
import Model.Sound;
import Model.Tea;
import Model.Actionable;
import Model.TimeCounter;

public class GamePlayPage extends FirstPage implements Observer {

	Bin bin;
	Tea cup;
	TimeCounter timer;

	Customer eiei;
	CustomerAction customerUI;
	Checker checker;

	SmartObject lidContainer;
	SmartObject cocoaSelection;
	SmartObject strawberrySelection;
	SmartObject greenTeaSelection;
	SmartObject milkTeaSelection;
	SmartObject cupStack;
	SmartObject honeyBottle;
	SmartObject bubbleSign;
	SmartObject puddingSign;
	SmartObject jellySign;
	Sound sound;
	
	JLabel scoreLabel;
	JLabel toppingTray;
	JLabel teaMachine;
	JLabel board;
	JLabel tray;
	JLabel timeLabel;
	JLabel machine;
	JLabel bg;
	JLabel counter;
	JLabel dialog;
	JLabel teaNeed;
	JLabel honeyNeed;
	JLabel toppingNeed;
	JLabel sweetLevel;
	
	
	public GamePlayPage() throws UnsupportedAudioFileException, IOException, LineUnavailableException {

		instanceAllComponents();
		addObserverAllObj();
		setUpAllObj();
		addAllObj();
		setVisible(true);

	}

	public void instanceAllComponents() throws UnsupportedAudioFileException, IOException, LineUnavailableException {
		
		scoreLabel = new JLabel("0");
		sound = new Sound("song/chocobo.wav");
		cup = new Tea();
		eiei = new Customer();
		teaNeed = new JLabel(new ImageIcon("image/" + eiei.getRequest().getTeaType() + ".png"));
		honeyNeed = new JLabel(new ImageIcon("image/honey.png"));
		toppingNeed = new JLabel(new ImageIcon("image/"+eiei.getRequest().getTopping()+".png"));
		sweetLevel = new JLabel(new ImageIcon("image/" + eiei.getRequest().getSweetness() + ".png"));
		customerUI = new CustomerAction(eiei.getCharacter(), eiei.getCharacter(),cup,eiei,toppingNeed,teaNeed,sweetLevel);
		jellySign = new Sign("image/jellysign.png", "image/jellysignrs.png", cup, "jelly");
		puddingSign = new Sign("image/puddingsign.png", "image/puddingsignrs.png", cup, "pudding");
		bubbleSign = new Sign("image/bubblesign.png", "image/bubblesignrs.png", cup, "bubble");
		toppingTray = new JLabel(new ImageIcon("image/toppingTray.png"));
		honeyBottle = new HoneyBottle("image/honeyBottle.png", "image/honeyBottlers.png", cup);
		milkTeaSelection = new FlavourSelector("image/milkteaFla.png","image/milkteaFlars.png",cup,"milktea");
		greenTeaSelection = new FlavourSelector("image/greenteaFla.png", "image/greenteaFlars.png",cup,"greentea");
		strawberrySelection = new FlavourSelector("image/strawberryFla.png","image/strawberryFlars.png",cup,"strawberry");
		cocoaSelection = new FlavourSelector("image/cocoaFla.png","image/cocoaFlars.png",cup,"cocoa");
		cupStack = new CupStack("image/cupStack.png", "image/cupStackrs.png",cup);
		lidContainer = new LidContainer("image/lid.png","image/lidOpen.png",cup);
		board = new JLabel(new ImageIcon("image/board.png"));
		timeLabel = new JLabel("60");
		bin = new Bin("image/bin.png","image/binOpen.png",cup);
		tray = new JLabel(new ImageIcon("image/tray.png"));
		checker = new Checker();
		teaMachine = new JLabel(new ImageIcon("image/teaMachine.png"));
		bg = new JLabel(new ImageIcon("image/bg_back.png"));
		counter = new JLabel(new ImageIcon("image/counter.png"));
		dialog = new JLabel(new ImageIcon("image/dialog.png"));
		timer =  new TimeCounter();
		timer.addObserver(this);

	}

	public void addAllObj() {
		
		add(scoreLabel);
		add(cup.getLidImg());
		add(cup.getCup());
		add(cup.getToppingImg());
		add(jellySign.getImage());
		add(puddingSign.getImage());
		add(bubbleSign.getImage());
		add(toppingTray);
		add(honeyBottle.getImage());
		add(greenTeaSelection.getImage());
		add(milkTeaSelection.getImage());		
		add(strawberrySelection.getImage());
		add(cocoaSelection.getImage());
		add(bin.getImage());
		add(lidContainer.getImage());
		add(cupStack.getImage());
		add(timeLabel);
		add(board);
		add(tray);
		add(teaMachine);
		add(counter);
		add(customerUI.getImage());
		add(sweetLevel);
		add(honeyNeed);
		add(toppingNeed);
		add(teaNeed);
		add(dialog);
		add(bg);
		
	}


	public void addObserverAllObj(){
		bin.addObserver(this);
		cupStack.addObserver(this);
		strawberrySelection.addObserver(this);
		cocoaSelection.addObserver(this);
		greenTeaSelection.addObserver(this);
		milkTeaSelection.addObserver(this);
		honeyBottle.addObserver(this);
		bubbleSign.addObserver(this);
		puddingSign.addObserver(this);
		jellySign.addObserver(this);
		lidContainer.addObserver(this);
		customerUI.addObserver(this);
		
	}
	
	public void setUpAllObj() {
//		scoreLabel.setForeground(Color.white);
		scoreLabel.setForeground(new Color(0xff8040));
		scoreLabel.setFont(new Font("bauhaus 93", Font.PLAIN, 60));
		scoreLabel.setBounds(822, 216,250,61);
		jellySign.getImage().setBounds(732, 359, 140, 138);
		puddingSign.getImage().setBounds(605, 359, 120, 138);
		bubbleSign.getImage().setBounds(463, 359, 140, 138);
		toppingTray.setBounds(425, 378, 464, 151);
		honeyBottle.getImage().setBounds(347, 334, 59, 190);
		teaMachine.setBounds(100, 210, 228, 311); // (47,51)
		milkTeaSelection.getImage().setBounds(teaMachine.getX()+156, teaMachine.getY()+33, 38, 55);
		greenTeaSelection.getImage().setBounds(teaMachine.getX()+118, teaMachine.getY()+33, 38, 55);
		strawberrySelection.getImage().setBounds(teaMachine.getX()+80, teaMachine.getY()+33, 38, 55);
		cocoaSelection.getImage().setBounds(teaMachine.getX()+42, teaMachine.getY()+33, 38, 55);
		lidContainer.getImage().setBounds(885, 358, 118, 135);
		cupStack.getImage().setBounds(7, 345, 103, 173);
		board.setBounds(786, 0, 212, 307);
		tray.setBounds(300, 541, 325, 105);
		bin.getImage().setBounds(840, 500, 117, 167);
		timeLabel.setBounds(868, 35, 168, 69);
		timeLabel.setFont(new Font("bauhaus 93", Font.PLAIN, 50));
		timeLabel.setForeground(new Color(0xff8040));
		bg.setBounds(0, 0, getWidth(), getHeight());
		counter.setBounds(0, 191, 1000, 700);
		customerUI.getImage().setBounds(540, 130, 230, 280);
		dialog.setBounds(280, 50, 320, 241);
		teaNeed.setBounds(dialog.getX() + 70, dialog.getY() + 45, 55, 55);
		honeyNeed.setBounds(dialog.getX() + 58, dialog.getY() + 103, 102, 41);
		toppingNeed.setBounds(dialog.getX() + 73, dialog.getY() + 142, 53, 43);
		sweetLevel.setBounds(dialog.getX() + 162, dialog.getY() + 93, 55, 55);
	}

	@Override
	public void update(Observable o, Object arg) {
		if (o instanceof TimeCounter) {
			if(!arg.equals("STOP"))
				timeLabel.setText(arg.toString());
			else{
				close();
				new ShowScorePage(customerUI.getScoreCounter().getScore());
				}
		}else{
			Actionable teaAction = (Actionable) arg;
			teaAction.doTeaCupAction(cup);
			scoreLabel.setText(customerUI.getScoreCounter().getScore()+"");
		}
	}
	
	

}
