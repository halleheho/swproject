package ViewController;

import java.io.IOException;
import java.util.Observable;
import java.util.Observer;

import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

import Model.Actionable;
import Model.HowToComponent;
import Model.PlaySign;
import Model.Sound;
import Model.Tea;

public class HowToPage extends FirstPage implements Observer{
	
	JLabel bg,instructionLabel; 
	HowToComponent step1,step2,step3,step4,step5,step6;
	PlaySign playSign;
	Sound song ;
	
	public HowToPage() {
		
		initiateAllobj();
		setUpAllObj();
		addAllObj(); 
		
	}
	
	public void initiateAllobj(){
		
		try {
			song = new Sound("song/congratulation.wav");
		} catch (UnsupportedAudioFileException | IOException
				| LineUnavailableException e) {
			e.printStackTrace();
		}
		instructionLabel = new JLabel();
		bg = new JLabel(new ImageIcon("image/howto/bg_howto.png"));
		step1 = new HowToComponent("step11", "step12");
		step2 = new HowToComponent("step21", "step22");
		step3 = new HowToComponent("step31", "step32");
		step4 = new HowToComponent("step41", "step42");
		step5 = new HowToComponent("step51", "step52");
		step6 = new HowToComponent("step61", "step62");
		playSign = new PlaySign("image/howto/play1.png", "image/howto/play2.png");
	}
	
	public void setUpAllObj(){
		
		bg.setBounds(0, 0, 1000, 700);
		instructionLabel.setBounds(342, 39, 618, 616);
		step1.getImage().setBounds(7, 34, 252, 108);
		step2.getImage().setBounds(7, 141, 265, 108);
		step3.getImage().setBounds(-5, 242, 252, 108);
		step4.getImage().setBounds(1, 345, 252, 108);
		step5.getImage().setBounds(7, 453, 268, 108);
		step6.getImage().setBounds(1, 556, 252, 108);
		playSign.getImage().setBounds(690, 565, 252, 108);
		
		step1.addObserver(this);
		step2.addObserver(this);
		step3.addObserver(this);
		step4.addObserver(this);
		step5.addObserver(this);
		step6.addObserver(this);
		playSign.addObserver(this);
				
	}

	public void addAllObj(){
		
		add(playSign.getImage());
		add(instructionLabel);
		add(step1.getImage());
		add(step2.getImage());
		add(step3.getImage());
		add(step4.getImage());
		add(step5.getImage());
		add(step6.getImage());
		add(bg);
		setVisible(true);
		
	}



	@Override
	public void update(Observable o, Object arg) {
		// TODO Auto-generated method stub
		if (o instanceof PlaySign){
			this.dispose();
			song.getClip().stop();
			try {
				new GamePlayPage();
			} catch (UnsupportedAudioFileException | IOException
					| LineUnavailableException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		instructionLabel.setIcon(new ImageIcon("image/howto/"+arg+".png"));
	}



}
