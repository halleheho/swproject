package ViewController;

import java.awt.Color;
import java.awt.Font;
import java.io.IOException;

import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

import Model.Sound;

public class ShowScorePage extends FirstPage{

	int score;
	JLabel scoreLabel,bg,youGot;
	
	public ShowScorePage(int score) {
		try {
			new Sound("song/congratulation.wav");
		} catch (UnsupportedAudioFileException | IOException
				| LineUnavailableException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		this.score = score;
		
		instanceAllObj();
		setUpAllObj();
		addAllObj();
		setVisible(true);
	}
	
	
	public void instanceAllObj(){
		youGot = new JLabel("You Got");
		bg = new JLabel(new ImageIcon("image/score.gif"));
		scoreLabel = new JLabel(score+" !");
	}
	
	public void setUpAllObj(){
		Font myFont = new Font("bauhaus 93", Font.PLAIN, 150);
		youGot.setFont(myFont);
		scoreLabel.setFont(myFont);
		scoreLabel.setForeground(Color.GREEN);
		youGot.setBounds(250, 100, 700, 200);
		scoreLabel.setBounds(300, 300, 600, 200);
		bg.setBounds(-15, -15, 1000, 700);
	}
	
	public void addAllObj(){
		add(youGot);
		add(scoreLabel);
		add(bg);
	}

}
