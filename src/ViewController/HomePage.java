package ViewController;

import java.io.IOException;
import java.util.Observable;
import java.util.Observer;

import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

import Model.ExitSign;
import Model.HomePageComponent;
import Model.HowToComponent;
import Model.HowtoSign;
import Model.PlaySign;
import Model.Sound;

public class HomePage extends FirstPage implements Observer {
	
	private JLabel bg, home, sign;
	private Sound homeSound;
	private PlaySign playSign;
	private HowtoSign howtoSign;
	private ExitSign exitSign;

	public HomePage() {
		
		initiateAllobj();
		setUpAllObj();
		addAllObj();
		
	}
	
	
	
	public void initiateAllobj(){
		
		try {
			homeSound = new Sound("song/congratulation.wav");
		} catch (UnsupportedAudioFileException | IOException
				| LineUnavailableException e) {
			e.printStackTrace();
		}
		
		bg = new JLabel(new ImageIcon("image/home/bg_home.png"));
		home = new JLabel(new ImageIcon("image/home/thehome.gif"));
		sign = new JLabel(new ImageIcon("image/home/sign.gif"));
		playSign = new PlaySign("image/home/play1.png", "image/home/play2.png");
		howtoSign = new HowtoSign("image/home/howto1.png", "image/home/howto2.png");
		exitSign = new ExitSign("image/home/exit1.png", "image/home/exit2.png");

	}
	
	public void setUpAllObj(){
		

		home.setBounds(0, 0, 671, 700);
		bg.setBounds(0, 0, 1000, 700);
		sign.setBounds(676, 258, 324, 442);

		playSign.getImage().setBounds(750, 372, 152, 52);
		
		howtoSign.getImage().setBounds(713, 440, 221, 55);
		exitSign.getImage().setBounds(761, 510, 117, 54);
		
		playSign.addObserver(this);
		howtoSign.addObserver(this);
		exitSign.addObserver(this);
				
	}

	public void addAllObj(){
		
		add(playSign.getImage());
		add(howtoSign.getImage());
		add(exitSign.getImage());

		add(home);	
		add(sign);
		add(bg);
		setVisible(true);
		
	}


	@Override
	public void update(Observable o, Object arg) {

		if (o instanceof PlaySign){
			homeSound.getClip().stop();
			dispose();
			try {
				new GamePlayPage();
			} catch (UnsupportedAudioFileException | IOException
					| LineUnavailableException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else if (o instanceof HowtoSign){
			homeSound.getClip().stop();
			dispose();
			new HowToPage();
		}else if (o instanceof ExitSign) {
			System.exit(0);
		}
		
	}



}
