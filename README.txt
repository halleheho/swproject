Happy Tea Friend.

software requirement:
	JDK or JRE v.1.7

Video Show Case:
	http://www.youtube.com/watch?v=tDjt2ttZju4

Source Code:
	https://bitbucket.org/halleheho/swproject

	** if you want to run JAR file. Make sure that image and song folder are in the same directory with the JAR file. If not, the program can't display pictures and sounds. We currently don't know how to combine JAR with pictures and sounds. Sorry for the inconvenience.
 